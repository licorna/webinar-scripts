This repository is a companion for the [Streamline MongoDB Deployments
with OpenShift and
Kubernetes](https://www.mongodb.com/webinar/streamline-mongodb-deployments-with-openshift-and-kubernetes)
webinar. Here you'll find links to relevant documentation, code
samples and a way of reproducing everything we did during the webinar.

# Installation

## OpenShift

OpenShift installation is out of the scope of this document, but
you'll find everything you need
[here](https://www.okd.io/). Installation is straightforward, there
are a few options there.

For testing you can use ~minishift~ and for a production cluster I
advise
[openshift-ansible](https://github.com/openshift/openshift-ansible).

### Project

I recommend creating a custom `Project` (equivalent to Kubernetes
`Namespace`) in Openshift.

``` bash
oc new-project demo01
```

This will create a new `demo01` project and `oc` will use it as
default.

## Operator

The MongoDB Enterprise Operator installation repo can be found
[here](https://github.com/mongodb/mongodb-enterprise-kubernetes).

First thing is to install the CRDs and the operator itself.

``` bash
REPO="https://raw.githubusercontent.com/mongodb/mongodb-enterprise-kubernetes/master"

oc apply -f $REPO/crds.yaml
oc apply -f $REPO/mongodb-enterprise-openshift.yaml

```

We simply point `REPO` to our public Github repo and *apply* those two
files using

## Ops Manager

If you don't have Ops Manager installed I recommend you get a copy
from [here](https://www.mongodb.com/download-center/ops-manager) and
install it, we'll need access to Ops Manager to use the operator.

# Configuration

## Ops Manager Credentials

We'll create a `Secret` object in Openshift to handle our Ops Manager
Credentials, for this we'll need to pieces of information:

* `user`: The username used to login to Ops Manager (it can be an
  email or simply a username)
* `publicApiKey`: An Ops Manager API Key. For more information about
  getting the API Key, please consult this
  [documentation](https://docs.opsmanager.mongodb.com/current/tutorial/configure-public-api-access/).

[Reference
docs](https://docs.opsmanager.mongodb.com/current/tutorial/install-k8s-operator/index.html#create-credentials).

If you want to automate these secret creation, you can use something
along the lines of:

``` bash
oc create secret generic my-credentials \
    --from-literal=user="<my-user>" \
    --from-literal=publicApiKey="<my-api-key>"
```

## Ops Manager Project

The second piece of configuration is a `ConfigMap` which will contain
information of our Ops Manager installation and which project we want
to use.

* `baseUrl`: This is, `host:port` of Ops Manager (ex:
  `http://my-ops-manager-host:8080`)
* `projectName`: The name of the project in Ops Manager to refer to
  this deployment.

[Reference docs](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/).

To automate this process:

``` bash
oc create configmap my-project \
    --from-literal=projectName="<my-project>" \
    --from-literal=baseUrl="<my-ops-manager-host-and-port>"
```

# Creating our first Replica Set

We'll start with something simple a MongoDB Replica Set with 3 members

> Please note this is not a Kubernetes Replica Set which is another
object type in Kubernetes that only shares its name with this
particular MongoDB topology.

## Base Replica Set Definition

[Reference
Docs](https://docs.opsmanager.mongodb.com/current/tutorial/deploy-replica-set/).

The simplest Replica Set is one with three members, no persistence (not
recommended for production environments) and whatever MongoDB version
we decide. A sample of it is:

``` yaml
# this file is replica-set-simple.yaml
---
apiVersion: mongodb.com/v1
kind: MongoDbReplicaSet
metadata:
  name: replica-set-simple
spec:
  members: 3
  version: 4.0.0

  project: my-project
  credentials: my-credentials

  persistent: false
```

This will create a 3 member Replica Set with MongoDB version `4.0.0`
(we'll update it later). Will be created in the Ops Manager *project*
described in the `project` attribute and will use the credentials
pointed at by the `credentials` attribute. For now we'll set the
`persistent` attribute to `false`, which means that the *disk* on each
container will be volatile and will dissapear after a Pod restart. To
send this *MongoDbReplicaSet* object to Kubernetes we have to run the
following command:

``` bash
oc apply -f replica-set-simple.yaml
```

The `oc` command will repond with a `mongodbreplicaset
"replica-set-simple" created` message. The new MongoDb deployment
should be ready in a few seconds and visible in OpenShift *Overview*
of your *Project*.

Visit Ops Manager *Project* this time to check the state of the new
Replica Set.

## Making modification

There are many ways of making modifications to a CRD and have the
operator apply these changes, the easiest one is to modify the source
`yaml` file and re-`apply` it. In this example we'll change the
`members` attribute from 3 to 5.

``` yaml
# this file is replica-set-simple.yaml
---
apiVersion: mongodb.com/v1
kind: MongoDbReplicaSet
metadata:
  name: replica-set-simple
spec:
  members: 5  # we only change this attribute
  version: 4.0.0

  project: my-project
  credentials: my-credentials

  persistent: false
```

And we run exactly the same command as before. We are *declaring* to
Kubernetes what is the state we want to reach, this is, to have a 5
members in the *Replica Set*. The operator will be informed of this
change and will proceed to configure the Replica. We don't have to
specify what to do, but where we want to get.

``` bash
oc apply -f replica-set-simple.yaml
```

If we get a list of the Pods we'll see that there are two new Pods
being created or initialized:

``` bash
$ oc get pods
NAME                                          READY     STATUS    RESTARTS   AGE
mongodb-enterprise-operator-957444799-xf7c6   1/1       Running   0          2d
replica-set-simple-0                          1/1       Running   0          2d
replica-set-simple-1                          1/1       Running   0          2d
replica-set-simple-2                          1/1       Running   0          2d
replica-set-simple-3                          1/1       Running   0          1m
replica-set-simple-4                          1/1       ContainerCreating   0          45s
```

We have two additional Pods that will hold the fourth and fifth
`mongod`s from our *Replica Set*.

## Patching the CRD to make modifications

There's another way of making changes to an existing `CRD`. We can
*patch* existing objects making modifications to the existing
structure using `jsonpatch` [syntax](http://jsonpatch.com/). As an
example, let's change the MongoDB version from `4.0.0` to `4.0.6`, the
latest MongoDB version at the time of this writting. No need to modify
any files, just run the following:

``` bash
oc patch mrs/replica-set-simple --type='json' -p='[{"op": "replace", "path": "/spec/version", "value": "4.0.6"}]'
```

By issuing this command, the `/spec/version` in the `patch` will
*replace* the current `4.0.0` value with the new `4.0.6`
value. There's nothing more to do, the Kubernetes cluster will inform
the operator about this change and the operator will know how to
peform this particular update.

# A more complex example

In this example we'll create the same *Replica Set* we created before,
but with a few additional attributes: a) we'll add a
`PersistentVolume` to the Pod so the data stored in disk will survive
a restart and b) we'll specify CPU & RAM requirements for this Pod to
run.

## Simple Replica Set with Persistent Volumes

There are multiple configuration options for Pods which can be found
[here](https://docs.opsmanager.mongodb.com/current/tutorial/deploy-replica-set/#add-any-additional-accepted-settings-for-a-replica-set-deployment). We
are not going to go over all of them, but instead, present an example
you can use yourself, with sane defaults:

``` yaml
# this file is replica-set-extended.yaml
---
apiVersion: mongodb.com/v1
kind: MongoDbReplicaSet
metadata:
  name: replica-set-ext
spec:
  members: 3
  version: 4.0.0

  project: my-project
  credentials: my-credentials

  # We'll use persistence, a PersistentVolume will be attach to this
  # Pod, if the Pod restarts, this data will not be lost.
  persistent: true
  
  # Here's where we define our Pod's specs
  podSpec:
  
    # Each Pod will have 1 CPU unit and 2 Gigabytes of RAM.
    # more info: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
    cpu: '1'
    memory: 2GB

    # "multiple" persistence allows to mount different directories to
    # different Persistent Volumes
    # In this case, the `data`, `journal` and `logs` reside in
    # different PersistentVolumes
    persistence:
      multiple:
        data:
          storage: 10Gi
        journal:
          storage: 1Gi
          labelSelector:
            matchLabels:
              app: "my-app"
        logs:
          storage: 500M
          storageClass: standard

    # For podAffinity and nodeAffinity see Kubernetes Docs
    # https://kubernetes.io/docs/concepts/configuration/assign-pod-node/
    podAntiAffinityTopologyKey: nodeId
    podAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: security
            operator: In
            values:
            - S1
        topologyKey: failure-domain.beta.kubernetes.io/zone
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: kubernetes.io/e2e-az-name
            operator: In
            values:
            - e2e-az1
            - e2e-az2

```

Applying this `yaml` file is as easy as to apply the *Replica Set
simple* one:

``` bash
oc apply -f replica-set-ext.yaml
```

For this command to succeed, you need a `StorageClass` with name
`standard` in your cluster or the request for `PersistentVolume`s
won't be completed by Kubernetes, hence the deploy will never reach
goal state.

# Resources

* [Ops Manager](https://www.mongodb.com/products/ops-manager) and
  [downloads](https://www.mongodb.com/download-center/ops-manager).
* [MongoDB Enterprise Operator
  Installation](https://docs.opsmanager.mongodb.com/current/tutorial/install-k8s-operator/)
  -- Official MongoDB docs.
* [okd.io](https://www.okd.io/) - Origin Distribution of Kubernetes
  (base of Openshift).

# Appendix

* `CustomResourceDefinition`
  [doc](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/):
  new custom resources that Kubernetes can create. There are custom
  [controllers](https://github.com/kubernetes/sample-controller) that
  can be *installed* to manage CRD, in this case they are called
  [operators](https://coreos.com/operators/).

